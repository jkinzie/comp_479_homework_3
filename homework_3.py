import numpy as np
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn import *
from collections import Counter
import matplotlib.pyplot as plt
from matplotlib.colors import ListedColormap

# https://www.kaggle.com/rajeevw/ufcdata

def normalize_data(d):
    n = np.copy(d)
    min_max_scaler = preprocessing.MinMaxScaler()
    np_scaled = min_max_scaler.fit_transform(n)
    df_normalized = pd.DataFrame(np_scaled)
    df_normalized.to_csv('normalized.csv')
    return df_normalized


def shuffle_data(d):
    s = shuffle(d, random_state=12345)
    # s = d.sample(frac=1).reset_index(drop=True)
    return s


def separator():
    print(50 * '=')
    print(50 * '-')


# import data as all floating
data = pd.read_csv('preprocessed_data_no_headers_condensed.csv', dtype=np.float64) # , usecols=np.arange(0,30)
# data = pd.read_csv('preprocessed_data_no_headers.csv')

# normalize data
data = normalize_data(data)

# shuffle data
data = shuffle_data(data)

# gives number of row count
print("Number of rows : ", data.shape[0])
# gives number of col count
print("Number of columns : ", data.shape[1])

# number of rows reserved for training
training_cutoff_rows = 2493 # 2493
dev_cutoff_rows = 3043 # 3043
cutoff_rows =  data.shape[0]

# training data set
train = data.iloc[:training_cutoff_rows]
train_x = data.iloc[:training_cutoff_rows, 1:]
train_y = data.iloc[:training_cutoff_rows, 0]
# test data set
dev = data.iloc[training_cutoff_rows:dev_cutoff_rows]
dev_x = data.iloc[training_cutoff_rows:dev_cutoff_rows, 1:]
dev_y = data.iloc[training_cutoff_rows:dev_cutoff_rows, 0]
# test data set
test = data.iloc[dev_cutoff_rows:data.shape[0]]
test_x = data.iloc[dev_cutoff_rows:cutoff_rows, 1:]
test_y = data.iloc[dev_cutoff_rows:cutoff_rows, 0]


#############################################################################
# Logistic Regression
separator()
print("PART 1 - Sklearn Logistic Regression")

logReg = LogisticRegression()
logReg.fit(train_x, train_y)

print("The percent predictability of sklearn's logistic regression is : ", logReg.score(dev_x, dev_y) * 100)

#############################################################################
# Logistic Regression Tweaking
separator()
print("PART 2 - Sklearn Logistic Regression Tweaking")

plt.figure()
performance = pd.DataFrame()
loop = 1000
C = .01
maxLogRegScore = -1
maxC = -1
while loop > 0:
    logReg = LogisticRegression(C=C, random_state=12345)  # , max_iter=1000
    logReg.fit(train_x, train_y)
    logRegScore = logReg.score(dev_x, dev_y) * 100
    print("The percent predictability of sklearn's logistic regression is : ",logRegScore )

    C = C + .01

    if logRegScore > maxLogRegScore:
        maxLogRegScore = logRegScore
        maxC = C

    tdf = pd.DataFrame([C,logRegScore])
    performance.append(tdf)
    plt.scatter(C, logRegScore, marker='o')
    loop -= 1

print("")
print("The maximum C value is : ",maxC," The Maximum score is : ", maxLogRegScore)
print("")

# plt.scatter(performance[:, 0], performance[:, 1], c=y, cmap=cmap, edgecolor='k', s=20)
plt.show()


#############################################################################
# K Nearest Neighbors
separator()

knn = KNeighborsClassifier()
knn.fit(train_x, train_y)
print("The percent predictability of sklearn's K Nearest Neighbors is : ", knn.score(test_x, test_y) * 100)

#############################################################################
# KNN Development and Tuning
separator()
print("")
print("PART 3 - KNN Development and Tuning")


def accuracy(y_true, y_pred):
    acc = np.sum(y_true == y_pred) / len(y_true)
    return acc


def euclidean_distance(p1, p2):
    return np.linalg.norm(np.array(p1) - np.array(p2))


class JayKNN:

    def __init__(self, k=3):
        self.k = k

    def fit(self, X, y):
        self.X_train = X
        self.y_train = y

    def predict(self, X):
        y_pred = [self._predict(x) for x in X]
        return np.array(y_pred)

    def _predict(self, x):
        # Compute distances between examples
        distances = [euclidean_distance(x, x_train) for x_train in self.X_train]
        # Sort by distance and return indices of the first k neighbors
        k_idx = np.argsort(distances)[:self.k]
        # Extract the labels
        k_neighbor_labels = [self.y_train[i] for i in k_idx]
        # return the most common class label
        most_common = Counter(k_neighbor_labels).most_common(1)
        return most_common[0][0]

# convert to numpy arrays
np_train_x = train_x.values
np_train_y = train_y.values
# convert to numpy arrays
np_dev_x = dev_x.values
np_dev_y = dev_y.values
# convert to numpy arrays
np_test_x = test_x.values
np_test_y = test_y.values


plt.figure()
maxKknScore = -1
maxK = -1
loop = 15
while loop > 0:
    k = loop # 3
    j_knn = JayKNN(k=k)
    j_knn.fit(np_train_x, np_train_y)
    predictions = j_knn.predict(np_dev_x)
    print("The percent predictability of Jay's K Nearest Neighbors is : ", accuracy(np_dev_y, predictions))
    kknScore = accuracy(np_dev_y, predictions)

    if kknScore > maxKknScore:
        maxKknScore = kknScore
        maxK = loop

    plt.scatter(loop, kknScore, marker='o')
    loop -= 1

print("")
print("The maximum K value is : ",maxK," The Maximum score is : ", maxKknScore)
print("")

plt.show()


#############################################################################
# Comparison Of Tuned Algorithms
separator()
print("")
print("PART 4 - Comparison Of Tuned Algorithms")

k = maxK
j_knn = JayKNN(k=k)
j_knn.fit(np_train_x, np_train_y)
predictions = j_knn.predict(np_test_x)
print("The percent predictability of Jay's tuned K Nearest Neighbors is : ", accuracy(np_test_y, predictions))
kknScore = accuracy(np_test_y, predictions)

logReg = LogisticRegression(C=maxC, random_state=12345)
logReg.fit(train_x, train_y)
logRegScore = logReg.score(test_x, test_y) * 100
print("The percent predictability of sklearn's tuned logistic regression is : ", logRegScore)

